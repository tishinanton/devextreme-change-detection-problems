import {Component, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements  OnChanges {
  title = 'devextreme-problems';
ngOnChanges(changes: SimpleChanges): void {
    console.log('changes');
}
}
