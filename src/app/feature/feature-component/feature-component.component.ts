import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-feature-component',
  templateUrl: './feature-component.component.html',
  styleUrls: ['./feature-component.component.scss']
})
export class FeatureComponentComponent implements OnInit {

    private _test = 1;
    public get test() {
        console.log(Math.random());
        return this._test;
    }

  constructor() { }

  ngOnInit(): void {
  }

}
