import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FeatureRoutingModule } from './feature-routing.module';
import { FeatureComponentComponent } from './feature-component/feature-component.component';
import {DxDataGridModule} from "devextreme-angular";


@NgModule({
  declarations: [
    FeatureComponentComponent
  ],
  imports: [
    CommonModule,
    FeatureRoutingModule,
      DxDataGridModule
  ]
})
export class FeatureModule { }
